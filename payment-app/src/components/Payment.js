import React, { useState } from "react";
import CustomerName from "./CustomerName";
import PaymentDetails from "./PaymentDetails";
import ShowHideButton from "./ShowHideButton";

const Payment = ({ id, paymentDate, type, amount, customerId }) => {
  const [visible, setVisibity] = useState(true);

  return (
    <div className="col-md-4">
      <div className="card mb-4 box-shadow" style={{ width: "18rem" }}>
        <div className="card-body">
          <CustomerName customerId={customerId} />
          <PaymentDetails
            id={id}
            paymentDate={paymentDate}
            type={type}
            amount={amount}
            customerId={customerId}
            visible={visible}
          />
          <ShowHideButton toggle={setVisibity} visible={visible} />
        </div>
      </div>
    </div>
  );
};

export default Payment;
