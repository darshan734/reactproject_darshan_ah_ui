import React from "react";

const PaymentDetails = ({
  id,
  paymentDate,
  type,
  amount,
  customerId,
  visible,
}) => {
  return (
    visible && (
      <ol className="album-tracks">
        <li key={id}>ID: {id}</li>
        <li key={paymentDate}>PaymentDate: {paymentDate}</li>
        <li key={type}>Type: {type}</li>
        <li key={amount}>Amount: {amount}</li>
        <li key={customerId}>customerId: {customerId}</li>
      </ol>
    )
  );
};

export default PaymentDetails;
