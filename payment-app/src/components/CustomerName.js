import React from "react";

const CustomerName = ({ customerId }) => (
  <div>
    {/* <h3 className="album-artist card-subtitle mb-2 text-muted">
      {props.name.id}
    </h3> */}
    <h3 className="album-artist card-subtitle mb-2 text-muted">
      Customer ID: {customerId}
    </h3>
  </div>
);

export default CustomerName;
