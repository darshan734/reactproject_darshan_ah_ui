import React from "react";
import Payment from "./Payment";
import { useSelector } from "react-redux";

const PaymentApiStatus = () => {
  const apiStatus = useSelector((state) => state.apiStatus.entities);
  console.log(apiStatus);
  const error = useSelector((state) => state.apiStatus.error);
  const loading = useSelector((state) => state.apiStatus.loading);

  if (error) {
    return <div className="d-flex justify-content-center">{error.message}</div>;
  }

  if (loading) {
    return (
      <div className="d-flex justify-content-center">
        <div
          className="spinner-border m-5"
          style={{ width: "4rem", height: "4rem" }}
          role="status"
        >
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-8">
          <div className="card mb-4 box-shadow" style={{ width: "40rem" }}>
            <h1>
              {/* // <div className="card-body">{apiStatus}</div> */}
              <span class="badge badge-secondary">{apiStatus}</span>
            </h1>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PaymentApiStatus;
