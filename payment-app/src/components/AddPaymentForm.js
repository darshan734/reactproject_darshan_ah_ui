import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { addPayment } from "../actions";

const AddPaymentForm = ({ fetchPayment, setFetchPayment }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [id, setId] = useState("");
  const [paymentDate, setPaymentDate] = useState("");
  const [type, setType] = useState("");
  const [amount, setAmount] = useState("");
  const [customerId, setCustomerId] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    // dispatch addPayment action
    dispatch(
      addPayment({
        id: id,
        paymentDate: paymentDate,
        type: type,
        amount: amount,
        customerId: customerId,
      })
    )
      .then(() => {
        console.log("addPayment is successful");
        setFetchPayment(!fetchPayment);
      })
      .catch(() => {})
      .finally(() => {
        console.log("addPayment thunk function is completed");
        history.push("/");
      });
  };

  return (
    <div className="container" style={{ marginTop: 10, marginBottom: 150 }}>
      <h3>Add New Payment</h3>
      <form onSubmit={handleSubmit} autoComplete="off">
        <div className="form-row">
          <div className="form-group col-md-5">
            <label htmlFor="id">ID:</label>
            <input
              id="id"
              type="number"
              className="form-control"
              value={id}
              onChange={(e) => setId(e.target.value)}
            />
          </div>
          <div className="form-group col-md-5">
            <label htmlFor="name">PaymentDate:</label>
            <input
              id="type"
              type="date"
              className="form-control"
              value={paymentDate}
              onChange={(e) => setPaymentDate(e.target.value)}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-md-5">
            <label htmlFor="type">Type:</label>
            <input
              id="type"
              type="text"
              className="form-control"
              value={type}
              onChange={(e) => setType(e.target.value)}
            />
          </div>
          <div className="form-group col-md-5">
            <label htmlFor="amount">Amount:</label>
            <input
              id="amount"
              type="number"
              className="form-control"
              value={amount}
              onChange={(e) => setAmount(e.target.value)}
            />
          </div>
          <div className="form-group col-md-5">
            <label htmlFor="customerId">CustomerID:</label>
            <input
              id="customerId"
              type="number"
              className="form-control"
              value={customerId}
              onChange={(e) => setCustomerId(e.target.value)}
            />
          </div>
        </div>
        <div className="form-group">
          <input type="submit" value="Add Payment" />
        </div>
      </form>
    </div>
  );
};

export default AddPaymentForm;
