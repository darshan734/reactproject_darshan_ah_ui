import ShowHideButton from "./ShowHideButton";
import React from "react";
import { act } from "react-dom/test-utils";
import { render, unmountComponentAtNode } from "react-dom";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("Toggle button clicked when visible is true", () => {
  const setVisible = jest.fn();
  act(() => {
    render(<ShowHideButton visible={true} toggle={setVisible} />, container);
  });
  const button = document.querySelector(".btn");
  expect(button.innerHTML).toBe("Hide");
});

it("Toggle button clicked when visible is false", () => {
  const setVisible = jest.fn();
  act(() => {
    render(<ShowHideButton visible={false} toggle={setVisible} />, container);
  });
  const button = document.querySelector(".btn");
  expect(button.innerHTML).toBe("Show");
});
