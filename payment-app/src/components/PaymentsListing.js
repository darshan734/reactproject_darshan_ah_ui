import React from "react";
import Payment from "./Payment";
import { useSelector } from "react-redux";

const PaymentsListing = () => {
  const payments = useSelector((state) => state.payments.entities);
  const error = useSelector((state) => state.payments.error);
  const loading = useSelector((state) => state.payments.loading);

  if (error) {
    return <div className="d-flex justify-content-center">{error.message}</div>;
  }

  if (loading) {
    return (
      <div className="d-flex justify-content-center">
        <div
          className="spinner-border m-5"
          style={{ width: "4rem", height: "4rem" }}
          role="status"
        >
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  }

  return (
    <div className="container">
      <div className="row">
        {payments.map((payment) => (
          <Payment
            id={payment.id}
            paymentDate={payment.paymentDate}
            type={payment.type}
            amount={payment.amount}
            customerId={payment.customerId}
          />
        ))}
      </div>
    </div>
  );
};

export default PaymentsListing;
