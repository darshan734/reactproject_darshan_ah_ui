import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import Banner from "./Banner";
import Navbar from "./Navbar";
import PaymentsListing from "./PaymentsListing";
import AddPaymentForm from "./AddPaymentForm";
import { fetchPayments, getApiStatus } from "../actions"; // pick up index.js by default
import PaymentApiStatus from "./PaymentApiStatus";

const App = () => {
  const dispatch = useDispatch();
  const [fetchPayment, setFetchPayment] = useState(false);
  const [getStatus, setGetStatus] = useState(false);

  useEffect(() => {
    dispatch(fetchPayments()); // dispatch fetchPayments action
  }, [fetchPayment, fetchPayments]); // run this at start and whenever fetchPayment is changed

  useEffect(() => {
    dispatch(getApiStatus()); // dispatch getApiStatus action
  }, [getStatus, getApiStatus]); // run this at start and whenever getApiStatus is changed

  return (
    <Router>
      <div className="app">
        <Navbar />
        <Switch>
          <Route exact path="/">
            <Banner />
            <PaymentsListing />
          </Route>
          <Route path="/add">
            <AddPaymentForm
              fetchPayment={fetchPayment}
              setFetchPayment={setFetchPayment}
            />
          </Route>
          <Route path="/about">
            <PaymentApiStatus />
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

export default App;
