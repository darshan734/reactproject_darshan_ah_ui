import axios from "axios";

// for our Action Creators
export const fetchPaymentsBegin = () => {
  return {
    type: "FETCH_PAYMENTS_BEGIN",
  };
};

export const fetchPaymentsSuccess = (payments) => {
  return {
    type: "FETCH_PAYMENTS_SUCCESS",
    payload: payments,
  };
};

export const fetchPaymentsFailure = (err) => {
  return {
    type: "FETCH_PAYMENTS_FAILURE",
    payload: { message: "Failed to fetch payments.. please try again later" },
  };
};

export const getApiStatusBegin = () => {
  return {
    type: "GET_APISTAUS_BEGIN",
  };
};

export const getApiStatusSuccess = (status) => {
  return {
    type: "GET_APISTATUS_SUCCESS",
    payload: status,
  };
};

export const getApiStatusFailure = (err) => {
  return {
    type: "GET_APISTATUS_FAILURE",
    payload: { message: "Failed to get staus.. please try again later" },
  };
};

// to be call by the components
export const fetchPayments = () => {
  // returns the thunk function
  return (dispatch, getState) => {
    dispatch(fetchPaymentsBegin());
    console.log("state after fetchPaymentsBegin", getState());
    axios.get("http://localhost:8080/api/payments/findAll").then(
      (res) => {
        setTimeout(() => {
          dispatch(fetchPaymentsSuccess(res.data));
          console.log("state after fetchPaymentsSuccess", getState());
        }, 3000);
      },
      (err) => {
        // dispatch FETCH_PAYMENTS_FAILURE
        dispatch(fetchPaymentsFailure(err));
        console.log("state after fetchPaymentsFailure", getState());
      }
    );
  };
};

export const addPaymentBegin = () => {
  return {
    type: "ADD_PAYMENT_BEGIN",
  };
};

export const addPaymentSuccess = () => {
  return {
    type: "ADD_PAYMENT_SUCCESS",
  };
};

export const addPaymentFailure = (err) => {
  return {
    type: "ADD_PAYMENT_FAILURE",
    payload: { message: "Failed to add new payment.. please try again later" },
  };
};

function delay(t, v) {
  return new Promise(function (resolve) {
    setTimeout(resolve.bind(null, v), t);
  });
}

export const addPayment = (payment) => {
  // returns our async thunk function
  return (dispatch, getState) => {
    return axios.post("http://localhost:8080/api/payments/save", payment).then(
      () => {
        console.log("payment created!");
        // this is where we can dispatch ADD_PAYMENT_SUCCESS
        dispatch(addPaymentSuccess());
      },
      (err) => {
        dispatch(addPaymentFailure(err));
        console.log("state after addPaymentFailure", getState());
      }
    );
  };
};

export const getApiStatus = () => {
  // returns the thunk function
  return (dispatch, getState) => {
    dispatch(getApiStatusBegin());
    console.log("state after getApiStatusBegin", getState());
    axios.get("http://localhost:8080/api/payments/apiStatus").then(
      (res) => {
        setTimeout(() => {
          dispatch(getApiStatusSuccess(res.data));
          console.log(res.data);
          console.log("state after getApiStatusSuccess", getState());
        }, 3000);
      },
      (err) => {
        // dispatch GET_APISTATUS_FAILURE
        dispatch(getApiStatusFailure(err));
        console.log("state after getApiStatusFailure", getState());
      }
    );
  };
};
