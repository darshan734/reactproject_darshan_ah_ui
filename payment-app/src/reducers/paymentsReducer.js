const initialState = { entities: [] };

export default (state = initialState, action) => {
  console.log(`received ${action.type} dispatch in paymentsReducer`);
  switch (action.type) {
    case "FETCH_PAYMENTS_BEGIN":
    case "ADD_PAYMENT_BEGIN":
      // case "GET_APISTATUS_BEGIN":
      return { ...state, loading: true, error: null };
    case "FETCH_PAYMENTS_SUCCESS":
      // case "GET_APISTATUS_SUCCESS":
      return { ...state, entities: action.payload, loading: false };
    case "FETCH_PAYMENTS_FAILURE":
      // case "GET_APISTATUS_FAILURE":
      return { ...state, entities: [], loading: false, error: action.payload };
    case "ADD_PAYMENT_SUCCESS":
      return { ...state, loading: false };
    case "ADD_PAYMENT_FAILURE":
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};
