import { combineReducers } from "redux";
import paymentsReducer from "./paymentsReducer";
import errorsReducer from "./errorsReducer";
import apiStatusReducer from "./apiStatusReducer";

const rootReducer = combineReducers({
  payments: paymentsReducer,
  apiStatus: apiStatusReducer,
  errors: errorsReducer,
});

export default rootReducer;
