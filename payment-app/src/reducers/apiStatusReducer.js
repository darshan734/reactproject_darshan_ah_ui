const initialState = { entities: [] };

export default (state = initialState, action) => {
  console.log(`received ${action.type} dispatch in apiStatusReducer`);
  switch (action.type) {
    case "GET_APISTATUS_BEGIN":
      return { ...state, loading: true, error: null };
    case "GET_APISTATUS_SUCCESS":
      return { ...state, entities: action.payload, loading: false };
    case "GET_APISTATUS_FAILURE":
      return { ...state, entities: [], loading: false, error: action.payload };
    default:
      return state;
  }
};
